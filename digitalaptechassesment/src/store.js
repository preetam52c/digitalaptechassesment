import { createStore, compose, applyMiddleware } from 'redux';
// import RootReducer from './Reducers/RootReducer';
import thunk from 'redux-thunk';

const store = createStore(
    // RootReducer,
    compose(applyMiddleware(thunk))
);

export default store;
