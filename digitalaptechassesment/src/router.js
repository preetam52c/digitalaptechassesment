import { EditMusic } from "./Pages/Editmusic/EditMusic";
import { Playlist } from "./Pages/Playlist/Playlist";

export const routes = [
    {path: '/', component: <Playlist/>},
    {path: '/editMusic/:id', component: <EditMusic/>},
]