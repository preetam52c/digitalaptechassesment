import * as React from 'react';
import { IconButton, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, TablePagination } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import { useNavigate } from 'react-router';
import Swal from 'sweetalert2'
import { deleteMusicAPI, deleteUserAPI, getPaginatedMusicsApi } from '../reduxWorkflow/apis/music.api';

function createData(name, email, id) {
  return {
    name,
    email,
    id
  };
}

function Row(props) {
  const navigate = useNavigate();
  const { row } = props;
  const { refreshData } = props;

  return (
    <React.Fragment>
      <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>


        <TableCell component="th" scope="row">
          {row.name}
        </TableCell>
        <TableCell >{row.email}</TableCell>
        <TableCell align="right">
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => navigate(`editMusic/${row.id}`)}
          >
            <EditIcon />
          </IconButton>
          <IconButton
            aria-label="expand row"
            size="small"
            onClick={() => Swal.fire({
              title: 'Are you sure?',
              text: "You won't be able to revert this!",
              icon: 'warning',
              showCancelButton: true,
              confirmButtonColor: '#3085d6',
              cancelButtonColor: '#d33',
              confirmButtonText: 'Yes, delete it!'
            }).then(async (result) => {
              if (result.isConfirmed) {
                try {
                  console.log("row", row);
                  await deleteUserAPI(row.id)
                  await refreshData()
                  Swal.fire(
                    'Deleted!',
                    'Your file has been deleted.',
                    'success'
                  )

                } catch (error) {
                  Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: error.message,
                    footer: '<a href="">Why do I have this issue?</a>'
                  })                }

              }
            })}
          >
            <DeleteIcon />
          </IconButton>
        </TableCell>
      </TableRow>

    </React.Fragment>
  );
}


export default function PlaylistTable() {
  const [page, setPage] = React.useState(1);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);
  const [paginatedData, setPaginatedData] = React.useState({rows: [], totalDocs: 0})

  React.useEffect(async() => {
    await getUserData()
    return () => {

    }
  }, [page])
  const getUserData = async () => {
    try {
      const data = await getPaginatedMusicsApi(page, rowsPerPage);
      console.log(data);
      setPaginatedData({rows: data.docs?.length ? data.docs.map((e, i) => createData(e.name, e.email, e._id)) : [], totalDocs: data.totalDocs})
    } catch (error) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: error.message,
        footer: '<a href="">Why do I have this issue?</a>'
      })    }

  }


  const handleChangePage = (event, newPage) => {
    setPage(newPage +1);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(1);
  };


  return (
    <Paper>
      <TableContainer >
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>

              <TableCell>Name</TableCell>
              <TableCell >Email</TableCell>
              <TableCell align="right">Actions</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {console.log("paginatedData===>", paginatedData)}
            {paginatedData.rows.map((row) => (
              <Row key={row.email} row={row} refreshData={async () => getUserData()} />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={paginatedData.totalDocs}
        rowsPerPage={rowsPerPage}
        page={page - 1}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>


  );
}
