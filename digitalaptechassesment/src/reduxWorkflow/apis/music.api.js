import axios from 'axios';
import { getUrl, routers } from './endpoints'

export const getPaginatedMusicsApi = async (page, limit) => {
    console.log('In api', getUrl(`${getUrl(routers.users)}?page=${page}&limit=${limit}`))
    try {
        return await (await axios.get(`${getUrl(routers.users)}?page=${page}&limit=${limit}`)).data.response;
    } catch (error) {
        throw error.response.data
    }
}

export const getUserByIdApi = async (id) => {
    console.log('In api', getUrl(routers.userDataById))
    try {
        return await (await axios.get(`${getUrl(routers.userDataById)}/${id}`)).data.response;
    } catch (error) {
        throw error.response.data
    }
}

export const createUserAPI = async (data) => {
    console.log('In api', getUrl(routers.createUser))
    try {
        return await axios.post(getUrl(routers.createUser), data);
    } catch (error) {
        throw error.response.data
    }
}

export const updateUserAPI = async (userEmail, data) => {
    console.log('In api', getUrl(routers.updateUser))
    try {
        return await axios.put(`${getUrl(routers.updateUser)}/${userEmail}`, data);
    } catch (error) {
        throw error.response.data
    }
}

export const createMusicAPI = async (data) => {
    console.log('In api', getUrl(routers.createMusic))
    try {
        return await axios.post(getUrl(routers.createMusic), data);
    } catch (error) {
        throw error.response.data
    }
}

export const getMusicByUserEmailApi = async (email) => {
    console.log('In api', getUrl(routers.musicsByUser))
    try {
        return await (await axios.get(`${getUrl(routers.musicsByUser)}/${email}`)).data.response;
    } catch (error) {
        throw error.response.data
    }
}

export const deleteUserAPI = async (userId) => {
    console.log('In api', getUrl(routers.deleteUser))
    try {
        return await axios.delete(getUrl(routers.deleteUser),{headers: {userId}});
    } catch (error) {
        throw error.response.data
    }
}

export const deleteMusicAPI = async (musicId) => {
    console.log('In api', getUrl(routers.deleteMusic))
    try {
        return await axios.delete(getUrl(routers.deleteMusic), {headers: {musicId}});
    } catch (error) {
        throw error.response.data
    }
}




