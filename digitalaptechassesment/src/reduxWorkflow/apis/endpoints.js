export const base_URL = 'http://localhost:5000';  

export const routers = {
    musics: '/api/musics',
    users: '/api/users',
    userDataById: '/api/userDataById',
    createUser: '/api/createUser',
    createMusic: '/api/createMusic',
    updateUser: '/api/updateUser',
    deleteUser: '/api/deleteUser',
    deleteMusic: '/api/deleteMusic',
    musicsByUser: '/api/musicsByUser'
    
}

export const getUrl = (key) => {
    return base_URL + key;
} 