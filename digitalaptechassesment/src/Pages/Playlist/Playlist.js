import { Button } from '@mui/material'
import React, {useEffect, useState} from 'react'
import Table from '../../Components/Table'
import { getPaginatedMusicsApi } from '../../reduxWorkflow/apis/music.api'
import { useNavigate } from 'react-router-dom'


export function Playlist() {

    const navigate = useNavigate()

    return (
        <div style={{padding: 15}}>
            <Button style={{ marginTop: 15, background: '#1976d2', color: '#fff', padding: '8px 10px' }} variant="contained" onClick={() => navigate('editMusic/new')}>Create</Button>
            <Table />
        </div>
    )
}
