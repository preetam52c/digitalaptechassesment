import { Button, TextField, IconButton } from '@mui/material'
import DeleteIcon from '@mui/icons-material/Delete';
import Swal from 'sweetalert2';
import React, { Fragment, useEffect, useState } from 'react'
import { useNavigate, useParams } from 'react-router'
import { createUserAPI, deleteMusicAPI, getMusicByUserEmailApi, getUserByIdApi, updateUserAPI } from '../../reduxWorkflow/apis/music.api'
import { formFields } from './config'
import './styles.css'
import { DropzoneArea } from 'material-ui-dropzone';
import { base_URL } from '../../reduxWorkflow/apis/endpoints'

export function EditMusic() {
    const navigate = useNavigate()
    const params = useParams()
    const [formData, setFormData] = useState({})
    const [musics, setMusics] = useState([])


    useEffect(() => {
        if (params.id !== 'new') {
            getUserDetails()
        }
        return () => {

        }
    }, [])

    const getUserDetails = async () => {
        const data = await getUserByIdApi(params.id)
        const musicList = await getMusicByUserEmailApi(data.email)
        setFormData(data)
        setMusics(musicList)
    }

    const onChangeHandler = (event) => {
        const { name, value } = event.target;
        setFormData({ ...formData, [name]: value })
    }

    const formSubmitHandler = async (event) => {
        event.preventDefault();
        try {
            console.log("formDataformData", formData);
            const dataToUpload = new FormData()
            Object.keys(formData).forEach((key) => {
                if (key !== 'music') {
                    console.log("mango bite", key, formData[key]);

                    dataToUpload.append(key, formData[key]);
                }

            })
            if (formData?.music?.length) {
                formData.music.forEach((e) => {
                    dataToUpload.append('music', e)
                })
            }


            if (params.id === 'new')
                await createUserAPI(dataToUpload)
            else
                await updateUserAPI(formData.email, dataToUpload)
            navigate('/')
        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: error.message,
                footer: '<a href="">Why do I have this issue?</a>'
              })        }

    }

    const fileUploadHandler = (data) => {
        console.log("file data", data);
        setFormData({ ...formData, music: data })
    }
    return (
        <>
            <form onSubmit={formSubmitHandler} className='formcontainer'>
                {
                    (params.id === 'new' || Object.keys(formData).length) ? formFields.map((field, idx) =>
                        <Fragment key={idx}>
                            {field.type !== 'file' ?
                                <TextField
                                    defaultValue={formData[field.key] ? formData[field.key] : ''}
                                    type={field.type}
                                    style={{ marginBottom: 15, width: 300 }}
                                    key={field.key} name={field.key}
                                    label={field.label}
                                    variant="outlined"
                                    disabled={params.id === 'new' ? false : field.isDisabled}
                                    required={field.isRequired}
                                    onChange={onChangeHandler} />
                                :
                                <DropzoneArea acceptedFiles={['audio/*']} onChange={fileUploadHandler} />
                            }
                        </Fragment>

                    ) : <div>loading...</div>
                }
                <Button style={{ marginTop: 15, background: '#1976d2', color: '#fff', padding: '8px 10px' }} type='submit' variant="contained">Submit</Button>
            </form>
            <h4 className='mH-2em'>Your Musics</h4>
            {musics?.length ? <div className='mH-2em' >
                {
                    musics.map((music, idx) =>
                        <div  key={idx} style={{display: 'flex'}}>
                            <audio className='mH-2em' controls>
                                <source src={`${base_URL}/api/${music.music}`} type="audio/mpeg">
                                </source>
                            </audio>
                            <IconButton
                                aria-label="expand row"
                                size="small"
                                onClick={() => Swal.fire({
                                    title: 'Are you sure?',
                                    text: "You won't be able to revert this!",
                                    icon: 'warning',
                                    showCancelButton: true,
                                    confirmButtonColor: '#3085d6',
                                    cancelButtonColor: '#d33',
                                    confirmButtonText: 'Yes, delete it!'
                                }).then(async (result) => {
                                    if (result.isConfirmed) {
                                        try {
                                            await deleteMusicAPI(music._id)
                                            await getUserDetails()
                                            Swal.fire(
                                                'Deleted!',
                                                'Your file has been deleted.',
                                                'success'
                                            )

                                        } catch (error) {
                                            Swal.fire({
                                                icon: 'error',
                                                title: 'Oops...',
                                                text: error.message,
                                                footer: '<a href="">Why do I have this issue?</a>'
                                              })
                                        }

                                    }
                                })}
                            >
                                <DeleteIcon />
                            </IconButton>
                        </div>
                    )

                }

            </div> : null
            }


        </>
    )
}
