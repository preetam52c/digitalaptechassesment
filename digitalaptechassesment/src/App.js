import {BrowserRouter, Routes, Route} from 'react-router-dom'
import { routes } from './router';
function App() {
  return (
    <div className="App">
      <BrowserRouter>
      <Routes>
        {
          routes.map((route, idx) => 
          <Route
         key={idx}
         path={route.path}
         element={route.component}
         />

          )
        }
      </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
