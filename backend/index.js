const express = require('express');
const dotenv = require('dotenv');
const cors = require('cors')
const connectDB = require('./dbConnection');
const musicRouter = require('./src/user/user.route')
const path = require('path')

dotenv.config();
const app = express();

//connect to mongodb database
connectDB();
app.use(cors())

app.options('*', cors());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", '*' ); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
  })
app.use(express.json())
// app.use(express.static(__dirname + '/public'));

const serveMusic = (req, res, next) => {
  const musicPathArr = req.path.split('/')[1].split('-')
  if(musicPathArr[1] === 'music') {
    let middleware = express.static(path.join (__dirname, 'public', musicPathArr[0]));
    middleware(req, res, next);
  } else {
    next()
  }
  
}

// const serveMusic = (req, res, next) => {
//   console.log("hey there", req);
//   // if(req.path === '')
//   let middleware = express.static(path.join (__dirname, 'public', 'undefined'));
//       middleware(req, res, next);
// }

app.use('/api', serveMusic, musicRouter)
const PORT = process.env.PORT || 5000;

app.get('/', (req, res) => res.send({"hello": "world"}))

//starting the server
app.listen(PORT,() => console.log(`Server is running on PORT: ${PORT}`))