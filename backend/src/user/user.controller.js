const { musicModel, userModel } = require("./user.model");
const mongoose = require('mongoose');
const fs = require('fs')

const getAllUsers = async (req, res) => {
    try {
        const { page, limit } = req.query;
        const response = await userModel.paginate({}, { page, limit, lean: true })

        return res.status(200).send({ response })

    } catch (error) {
        return res.status(400).send({ error: error.message })

    }
}

const getUserById = async (req, res) => {
    try {
        const { _id } = req.params;
        const response = await userModel.findOne({ _id })

        return res.status(200).send({ response })

    } catch (error) {
        return res.status(400).send({ error: error.message })

    }
}

const createUser = async (req, res) => {

    try {
        let musics
        if (req.files?.length)
            musics = req.files.map((file) => `${file.filename}`);

        const { name, email } = req.body;
        const userToSaved = new userModel({ name, email });
        const response = await userToSaved.save();
        if (musics)
            await createMusic(response.email, musics)

        if (response)
            return res.status(200).send({ response })
        else
            throw res.status(400).send({ message: 'Something went wrong' })

    } catch (error) {
        return res.status(400).send({ message: error.message })

    }
}


const createMusic = async (userEmail, musics) => {

    try {
        const musicToSaved = musics.map((music) => {
            return { music, userEmail }
        })
        console.log("musicToSaved", musicToSaved);
        const musicSaved = musicModel.insertMany(musicToSaved);

        if (musicSaved)
            return musicSaved


    } catch (error) {
        throw error

    }
}

const updateUser = async (req, res) => {
    try {
        const { userEmail } = req.params;
        let musics
        if (req.files?.length)
            musics = req.files.map((file) => `${file.filename}`);

        let fieldsToUpdate = {};
        if (req.body.name) fieldsToUpdate.name = req.body.name;

        const updatedUser = await userModel.findOneAndUpdate({ email: userEmail }, fieldsToUpdate, { new: true, runValidators: true });
        if (musics)
            await createMusic(userEmail, musics)
        console.log(updatedUser);

        if (!updatedUser) return res.status(400).send({ error: `No music with musicId: ${userEmail}` });
        else res.status(200).send({ updatedMusic: updatedUser });

    } catch (error) {
        return res.status(400).send({ error: error.message })
    }
}

const deleteUser = async (req, res) => {
    try {
        const { userid } = req.headers;
        const isDeleted = await userModel.findOneAndDelete({ _id: userid })
        if (!isDeleted) return res.status(400).send({ message: `No user found with this userId: ${userid}` });

        // directory path
        const dir = `./public/${isDeleted.email}`
        if (fs.existsSync(dir)) {
            console.log('Directory exists!');
            fs.rm(dir, { recursive: true }, (err) => {
                if (err) {
                    throw err;
                }
    
                console.log(`${dir} is deleted!`);
            });
        }
        // delete directory recursively
        
        return res.status(200).send({ message: `User deleted with this userId: ${userid}` });
    } catch (error) {
        return res.status(400).send({ message: error.message })

    }
}

const deleteMusic = async (req, res) => {
    try {
        const { musicid } = req.headers;
        const isDeleted = await musicModel.findOneAndDelete({ _id: musicid })
        if (!isDeleted) return res.status(400).send({ message: `No music found with this musicId: ${musicid}` });

        const dir = `./public/${isDeleted.userEmail}/${isDeleted.music}`
        if (fs.existsSync(dir)) {
            console.log('Directory exists!');
            fs.unlink(dir, function (err) {
                if (err) throw err;
                // if no error, file has been deleted successfully
                console.log('File deleted!');
            });
        }
        
        return res.status(200).send({ message: `Music deleted with this musicId: ${musicid}` });
    } catch (error) {
        return res.status(400).send({ message: error.message })

    }
}

const getMusicByUserEmail = async (req, res) => {
    try {
        const { userEmail } = req.params;
        const response = await musicModel.find({ userEmail })
        return res.status(200).send({ response })
    } catch (error) {
        return res.status(400).send({ message: error.message })
    }
}

module.exports = {
    getAllUsers,
    getUserById,
    createUser,
    createMusic,
    updateUser,
    deleteUser,
    deleteMusic,
    getMusicByUserEmail
}