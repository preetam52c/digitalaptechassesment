const express = require('express');
const { createMusic, updateMusic, deleteMusic, getAllUsers, getUserById, createUser, updateUser, getMusicByUserEmail, deleteUser } = require('./user.controller');
const router = express.Router();
const multer = require("multer");
const path = require('path');
const fs = require('fs')

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        const dir = `./public/${req.body.email}`
        if (fs.existsSync(dir)) {
            console.log('Directory exists!');
        } else {
            fs.mkdir(dir, (err) => {
                if (err) {
                  console.log(err)
                } else {
                  console.log("New directory successfully created.")
                }
              })
        }
        cb(null, dir);
    },
    
    filename: function(req, file, cb) {

        cb(null, `${req.body.email}-${file.fieldname}-${Date.now().toString()}${path.extname(file.originalname)}` );
    }
});

const upload = multer({
    storage: storage,
    limits: { fileSize: 4 * 1024 * 1024 }
})

router.get('/users', getAllUsers)
router.get('/userDataById/:_id', getUserById)
router.post('/createUser', upload.any('music'), createUser)
router.put('/updateUser/:userEmail', upload.any('music'), updateUser);
router.get('/musicsByUser/:userEmail', getMusicByUserEmail)
router.delete('/deleteUser', deleteUser);

router.delete('/deleteMusic', deleteMusic);

module.exports = router;