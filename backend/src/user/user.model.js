const mongoose = require('mongoose');
const { Schema, Types } = mongoose;
const mongoosePaginate = require('mongoose-paginate-v2');

const UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    

})

const MusicSchema = new Schema({
    music: {
        type: String,
        required: true
    },
    userEmail: {
        type: String,
        required: true
    }
    
})
UserSchema.plugin(mongoosePaginate)
const userModel = mongoose.model('users', UserSchema);

const musicModel = mongoose.model('musics', MusicSchema);



module.exports = {
    musicModel,
    userModel
}